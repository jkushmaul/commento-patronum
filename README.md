# Less mangle

Just a less mangled way of running this on heroku.  There's no need to fork the source.

But - thanks to this person, https://github.com/fidiego/commento-heroku/blob/9b3a240dbd9580cdd97e9ad42c906cc360ea3324/app.json
for starting the idea.

## Heroku setup

I really whipped through this one, I make no guaruntees about this, other than it won't work out of the gate. 

Create your heroku app first to get the hostname  

`heroku create`

Configure `COMMENT_ORIGIN`

`heroku config:set COMMENTO_ORIGIN=$( heroku apps:info -s  | grep web_url | cut -d= -f2)`

## For Gitlab Oauth:

A nice undocumented feature is the oauth - a self service system with email registration by default but
you'll have a hard time getting users to comment (for all the users I don't have).

1. Visit https://gitlab.com, Account, Settings, Applications and create a new application.
2. Use a callback URL of: `https://${COMMENTO_ORIGIN}/api/oauth/gitlab/callback` (evaluate $COMMENTO_ORIGIN)
3. Take note of your key and secret for the similarly named items back into heroku.
   1. `heroku config:set COMMENTO_GITLAB_KEY=https://gitlab.com`
   2. `heroku config:set COMMENTO_GITLAB_URL=https://gitlab.com`

## Push

1. `git push heroku master`

## Complete

1. Visit `$( heroku apps:info -s  | grep web_url | cut -d= -f2)`.
2. Register as owner
3. Turn off new owners:

   * `heroku config:set COMMENTO_FORBID_NEW_OWNERS=true`
   * Restart your dyno just to be safe.
   * Try to register again.

## Finally

The slightly extended docker image available at this projects registry:

registry.gitlab.com/jkushmaul/commento-patronum:latest