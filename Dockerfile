FROM registry.gitlab.com/commento/commento:latest

# Since this could change with :latest
RUN test -e commento  || (echo 'commento/commento does not exist'; exit -1)
RUN test -x commento || (echo 'commento/commento is not executable'; exit -1)

#Only the last ENTRYPOINT instruction in the Dockerfile will have an effect.
ENTRYPOINT "export COMMENTO_PORT=\${PORT}; export COMMENTO_POSTGRES=\${DATABASE_URL}; ./commento"
